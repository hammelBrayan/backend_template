package com.mamaion.colis_plus_backend.handel;

import com.mamaion.colis_plus_backend.controllers.UserController;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.util.HtmlUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerWebSocketHandler extends TextWebSocketHandler {
    private Logger logger = Logger.getLogger(UserController.class.getName());
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String request = message.getPayload();
        logger.log(Level.INFO, request + session.getId());

        String response = String.format("response from server to '%s'", HtmlUtils.htmlEscape(request));
        logger.log(Level.INFO, response);

        session.sendMessage(new TextMessage(response));
    }
//    @Scheduled(fixedRate = 10000)
//    void sendPeriodicMessages() throws IOException {
//        for (WebSocketSession session : session) {
//            if (session.isOpen()) {
//                String broadcast = "server periodic message " + LocalTime.now();
//                logger.log(Level.INFO, broadcast);
//                session.sendMessage(new TextMessage(broadcast));
//            }
//        }
//    }
//    @Scheduled(fixedRate = 10000)
//    void sendPeriodicMessages() throws IOException {
//        for (WebSocketSession session : sessions) {
//            if (session.isOpen()) {
//                String broadcast = "server periodic message " + LocalTime.now();
//                logger.info("Server sends: {}", broadcast);
//                session.sendMessage(new TextMessage(broadcast));
//            }
//        }
//    }
}
