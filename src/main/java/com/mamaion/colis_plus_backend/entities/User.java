package com.mamaion.colis_plus_backend.entities;

import com.mamaion.colis_plus_backend.entities.audits.DateAudit;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class User extends DateAudit implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;
    private String email;
    private String phone;
    private String name;
    private String address;
    private String username;
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

//    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
//    private UserProfile userProfile;

    public User() {
    }

//    public User(String username, String password, Set<Role> roles, UserProfile userProfile) {
//        this.username = username;
//        this.password = password;
//        this.roles = roles;
//        this.userProfile = userProfile;
//    }
//
//    public User(String username, String password, UserProfile userProfile) {
//        this.username = username;
//        this.password = password;
//        this.userProfile = userProfile;
//    }

    public User(String email, String phone, String username, String password) {
        this.email = email;
        this.phone = phone;
        this.username = username;
        this.password = password;
    }

    public User(String email, String phone, String name, String address, String username, String password) {
        this.email = email;
        this.phone = phone;
        this.name = name;
        this.address = address;
        this.username = username;
        this.password = password;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

//    public UserProfile getUserProfile() {
//        return userProfile;
//    }
//
//    public void setUserProfile(UserProfile userProfile) {
//        this.userProfile = userProfile;
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
