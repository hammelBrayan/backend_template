package com.mamaion.colis_plus_backend.controllers;

import com.mamaion.colis_plus_backend.payloads.ApiResponse;
import com.mamaion.colis_plus_backend.payloads.JwtAuthenticationResponse;
import com.mamaion.colis_plus_backend.payloads.LoginRequest;
import com.mamaion.colis_plus_backend.security.JwtTokenProvider;
import com.mamaion.colis_plus_backend.services.RoleService;
import com.mamaion.colis_plus_backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestMapping("colis/auth")
@RestController
@CrossOrigin("*")
public class AuthController {
    private Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private RoleService roleService;

//    @Autowired
//    private UserProfileService userProfileService;

    @CrossOrigin("*")
    @PostMapping("/signin")

    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        System.out.println(loginRequest.getUsername() + "," + loginRequest.getPassword() + "," + this.passwordEncoder.encode(loginRequest.getPassword()));
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        try {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtTokenProvider.generateToken(authentication);
            String roleUser = userService.getUSerRole(loginRequest.getUsername());
            System.out.println(roleUser);
            return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, roleUser));
        } catch (Exception e) {
            return new ResponseEntity(new ApiResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @CrossOrigin("*")
//    @PostMapping("/signup")
//    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
//        try {
//            logger.log(Level.INFO, new ObjectMapper().writeValueAsString(signUpRequest));
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        try {
//
//            if (userService.existByUsername(signUpRequest.getUsername())) {
//                return new ResponseEntity(new ApiResponse(false, "Username is already taken"), HttpStatus.BAD_REQUEST);
//            }
//            if (userService.existByEmail(signUpRequest.getEmail())) {
//                return new ResponseEntity(new ApiResponse(false, "Email address already in use"), HttpStatus.BAD_REQUEST);
//            }
//            User user = new User(signUpRequest.getEmail() , signUpRequest.getPhone() , signUpRequest.getFirstName(),  signUpRequest.getAddress() , signUpRequest.getUsername() , signUpRequest.getPassword() );
//
//            Role userRole = roleService.findByName(RoleName.ROLE_CLIENT)
//                    .orElseThrow(() -> new AppException("User Role not set"));
//
//            user.setRoles(Collections.singleton(userRole));
////            userService.add(user);
//            user.setPassword(passwordEncoder.encode(user.getPassword()));
//            UserProfile userProfile =  new UserProfile(signUpRequest.getFirstName() , signUpRequest.getSecondName(), signUpRequest.getEmail(),
//                    signUpRequest.getPhone() , signUpRequest.getWhatsappNumber() , signUpRequest.getAddress() , signUpRequest.getProfil(),
//                    signUpRequest.getNationality() , signUpRequest.getDateOfBirth() , signUpRequest.getCurrency() , true ,
//                    signUpRequest.isCertifie(), signUpRequest.isCertifieEmail() , signUpRequest.isCertifiePhone() , signUpRequest.getGender(),
//                    signUpRequest.getPosition() , signUpRequest.getIdentification() , user);
//            userProfileService.save(userProfile);
//
//            return new ResponseEntity(new ApiResponse(true, "Utilisateur enregistré avec succes"), HttpStatus.CREATED);
//        } catch (Exception e) {
//            return new ResponseEntity(new ApiResponse(false, "Erreur d'enregistrement du cleint : " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//        //return new ResponseEntity(new ApiResponse(false, "Erreur d'enregistrement du cleint : " ), HttpStatus.INTERNAL_SERVER_ERROR);
//    }

//
//    @CrossOrigin("*")
//    @PostMapping("/signeddin")
//    public ResponseEntity<?> registerAdmin(@Valid @RequestBody SignUpRequest signUpRequest) {
//        try {
////            logger.log(Level.INFO, new ObjectMapper().writeValueAsString(signUpRequest));
////        } catch (JsonProcessingException e) {
////            e.printStackTrace();
////        }
////        if (userService.existByUsername(signUpRequest.getUsername())) {
////            return new ResponseEntity(new ApiResponse(false, "Username is already taken"), HttpStatus.BAD_REQUEST);
////        }
////        if (userService.existByEmail(signUpRequest.getEmail())) {
////            return new ResponseEntity(new ApiResponse(false, "Email address already in use"), HttpStatus.BAD_REQUEST);
////        }
////        User user = new User(signUpRequest.getUsername() , signUpRequest.getPassword() );
////
////        Role userRole = roleService.findByName(RoleName.ROLE_CLIENT)
////                .orElseThrow(() -> new AppException("User Role not set"));
////
////        user.setRoles(Collections.singleton(userRole));
////        userService.add(user);
////
////        UserProfile userProfile =  new UserProfile(signUpRequest.getFirstName() , signUpRequest.getSecondName(), signUpRequest.getEmail(),
////                signUpRequest.getPhone() , signUpRequest.getWhatsappNumber() , signUpRequest.getAddress() , signUpRequest.getProfil(),
////                signUpRequest.getNationality() , signUpRequest.getDateOfBirth() , signUpRequest.getCurrency() , true ,
////                signUpRequest.isCertifie(), signUpRequest.isCertifieEmail() , signUpRequest.isCertifiePhone() , signUpRequest.getGender(),
////                signUpRequest.getPosition() , signUpRequest.getIdentification());
////
//
//        return new ResponseEntity(new ApiResponse(true, "Compte admin crée avec succes enregistré avec succes"), HttpStatus.CREATED);
//    }
//

}
