package com.mamaion.colis_plus_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColisPlusBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ColisPlusBackendApplication.class, args);
    }

}
