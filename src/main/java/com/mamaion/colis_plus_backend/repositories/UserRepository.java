package com.mamaion.colis_plus_backend.repositories;


import com.mamaion.colis_plus_backend.entities.Role;
import com.mamaion.colis_plus_backend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    Optional<User> getUserByUsername(String username);
    @Query(value = "SELECT roles FROM  User u  WHERE u.username = :motcle ")
    public List<Role> getUSerRole(@Param("motcle") String motcle);

}
