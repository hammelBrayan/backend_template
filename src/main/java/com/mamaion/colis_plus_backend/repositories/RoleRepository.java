package com.mamaion.colis_plus_backend.repositories;

import com.mamaion.colis_plus_backend.entities.Role;
import com.mamaion.colis_plus_backend.utilities.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
