package com.mamaion.colis_plus_backend.services.serviceImpl;

import com.mamaion.colis_plus_backend.entities.User;
import com.mamaion.colis_plus_backend.repositories.RoleRepository;
import com.mamaion.colis_plus_backend.repositories.UserRepository;
import com.mamaion.colis_plus_backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {
    private Logger logger = Logger.getLogger(UserServiceImpl.class.getName());

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User add(User user) {
        try {

            return userRepository.save(user);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "une erreur s'est produite pendant la sauvegarde de l utilisateur", e);
            return null;
        }
    }
    @Override
    public String getUSerRole(String username) {
        return userRepository.getUSerRole(username).get(0).getName().toString();
    }

    @Override
    public boolean existByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public boolean existByEmail(String email) {
        return userRepository.existsByEmail(email);
    }


    @Override
    public List<User> listUser() {
        return userRepository.findAll();
    }

}
