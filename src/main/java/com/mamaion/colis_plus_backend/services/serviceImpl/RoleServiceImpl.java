package com.mamaion.colis_plus_backend.services.serviceImpl;

import com.mamaion.colis_plus_backend.entities.Role;
import com.mamaion.colis_plus_backend.repositories.RoleRepository;
import com.mamaion.colis_plus_backend.services.RoleService;
import com.mamaion.colis_plus_backend.utilities.RoleName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class RoleServiceImpl implements RoleService {

    private Logger logger = Logger.getLogger(RoleServiceImpl.class.getName());

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Role> findAll() {
        try {
            return roleRepository.findAll();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Une erreur s est prodiute lors de la recuperation des profiles", e);
            return null;
        }
    }

    @Override
    public Optional<Role> findByName(RoleName role) {
        return roleRepository.findByName(role);
    }


}
