package com.mamaion.colis_plus_backend.services;


import com.mamaion.colis_plus_backend.entities.Role;
import com.mamaion.colis_plus_backend.utilities.RoleName;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    List<Role> findAll();

    Optional<Role> findByName(RoleName role);
}
