package com.mamaion.colis_plus_backend.services;

import com.mamaion.colis_plus_backend.entities.User;

import java.util.List;

public interface UserService {
    User add(User user);
    boolean existByUsername(String username);
    boolean existByEmail(String email);
//    boolean existByEmail(String email);
    List<User> listUser();
    String getUSerRole(String username);

}
