package com.mamaion.colis_plus_backend.utilities;

public enum RoleName {
    ROLE_CLIENT,
    ROLE_ADMIN,
}
