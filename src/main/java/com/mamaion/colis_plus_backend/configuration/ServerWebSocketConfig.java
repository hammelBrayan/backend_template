package com.mamaion.colis_plus_backend.configuration;
//
import com.mamaion.colis_plus_backend.handel.ServerWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

//@Configuration
//@EnableWebSocketMessageBroker
//public class WebSocketChatConfig implements WebSocketMessageBrokerConfigurer {
//
//    @Override
//    public void configureMessageBroker(MessageBrokerRegistry config) {
//
//        // Enregistrer un message broker pour gérer les messages envoyés par les clients
//
//        config.enableSimpleBroker("/topic");
//
//        // Définir le préfixe pour les messages envoyés par le client (les clients enverront des messages à /app/)
//
//        config.setApplicationDestinationPrefixes("/app");
//
//    }
//
//    @Override   public void registerStompEndpoints(StompEndpointRegistry registry) {
//
//        // Enregistrer un point de terminaison STOMP pour que les clients puissent se connecter
//
//        registry.addEndpoint("/websocketApp");
//
//    }
////    @Override
////    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
////        String request = message.getPayload();
////        logger.info("Server received: {}", request);
////
////        String response = String.format("response from server to '%s'", HtmlUtils.htmlEscape(request));
////        logger.info("Server sends: {}", response);
////        session.sendMessage(new TextMessage(response));
////    }
//}


@Configuration
@EnableWebSocket
public class ServerWebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(webSocketHandler(), "/websocket");
    }

    @Bean
    public WebSocketHandler webSocketHandler() {
        return new ServerWebSocketHandler();
    }
}