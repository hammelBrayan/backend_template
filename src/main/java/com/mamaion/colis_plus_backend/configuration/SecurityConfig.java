package com.mamaion.colis_plus_backend.configuration;

import com.mamaion.colis_plus_backend.security.CustomUserDetailsService;
import com.mamaion.colis_plus_backend.security.JwtAuthentificationEntryPoint;
import com.mamaion.colis_plus_backend.security.JwtAuthentificationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * @author
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Autowired
    private JwtAuthentificationEntryPoint jwtAuthentificationEntryPoint;

    @Bean
    public JwtAuthentificationFilter jwtAuthentificationFilter() {
        return new JwtAuthentificationFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(customUserDetailsService);
//                .passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthentificationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/**/*.png",
                        "/**/*.jpg",
                        "/**/.css",
                        "/**/.js",
                        "/**/.html",
                        "/rest/**")
                .permitAll()
//                .antMatchers("/rapidoseat/auth/**")
//                .permitAll()
//                .antMatchers(HttpMethod.GET, "rapidoseat/auth/**")
//                .permitAll()
//                .antMatchers("/rapidoseat/auth/checkUsernameAvailability", "rapidoseat/auth/checkEmailAvailability")
//                .permitAll()
//                .antMatchers("/rapidoseat/auth/signin").permitAll()
//                .antMatchers("rapidoseat/auth/signup").permitAll()
//                .antMatchers("rapidoseat/categorie/tout").permitAll()
//                .antMatchers("rapidoseat/produit/all").permitAll()
//                .antMatchers("rapidoseat/cart/command").permitAll()
//        http.authorizeRequests().antMatchers("/admin/orderList", "/admin/order", "/admin/accountInfo")//
//                .access("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')"); only roles accessing to the resource
//        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403"); refusal for
                .anyRequest()
                .permitAll();

        // .authenticated();

        http.addFilterBefore(jwtAuthentificationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
